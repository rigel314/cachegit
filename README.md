cachegit
======

## About
cachegit's goal is to save disk space when using multiple repos with fork relationships and similar sets of submodules.

cachegit is a collection of bash scripts to use multiple repos but keep a single directory of git objects.  Each repo is unique except for the shared objects folder.

Automatic garbage collection is disabled and the repos are initially fetched with a partial clone filter that ignores all blobs.  I considered using git worktrees, but git worktrees don't allow multiple worktrees to have the same branch checked out.

## Usage
### Installation
cachgit should typically be installed with `ln -s "$(pwd)/git-cache" /usr/local/bin`. This will setup the custom git subcommand: `git cache`

### Subcommands
* `git cache setup`
  * a one-time setup to create your shared git objects folder
* `git cache clone`
  * works like `git clone` but does a partial clone using the shared git objects folder
* `git cache submodule init`
  * works like `git submodule init`, but internally calls `git cache clone` for each submodule
* `git cache submodule update`
  * a helper for `git submodule update`, but if `--init` is specified, `git cache submodule init` is used instead and `--init` is not passed to `git submodule update`
* `git cache submodule <subcmd>`
  * just a helper for `git submodule <subcmd>`

### Dependencies
* a recent version of git
  * partial clone support
* bash
* realfile
* flock
* git servers that support partial clones

### Scripts
* initialsetup.sh
  * creates the main cachegit folder along with a placeholder directory for git objects and union repo for running manual garbage collects
* newrepo.sh
  * clones a new repo after setting up the shared objects directory
* submoduleinit.sh
  * initializes submodules (optionally recursively), by calling newrepo.sh after resolving any relative submodule URLs
* gc.sh
  * tries delete unreferenced blobs by looking at the blobs referenced by all checkouts (basically tries to recreate the original partial clone)
  * tries to save space by garbage collecting in the union repo
