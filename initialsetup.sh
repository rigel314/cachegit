#!/bin/bash

if [[ -f ~/.config/cachegit/settings ]];then
	echo "cachgit already setup"
	exit 1
fi

cachedir="~/.config/cachegit"

while [[ -n "$1" ]]; do
	case "$1" in
	-h | --help)
		echo "TODO: add help"
		exit 0
		;;
	-d | --dir)
		shift
		cachedir="$1"
		;;
	esac

	shift
done

mkdir -p "$cachedir/obj/info"
mkdir -p "$cachedir/obj/pack"
mkdir -p "$cachedir/union.git"

cd "$cachedir/union.git"
git init --bare
git config gc.auto 0
rm -rf objects
ln -s ../obj objects

touch "$cachedir/list"

cat <<EOF >~/.config/cachegit/settings
cachedir="$cachedir"
EOF
