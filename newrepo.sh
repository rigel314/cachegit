#!/bin/bash

if [[ ! -f ~/.config/cachegit/settings ]]; then
	echo 'cachgit not setup, run initialsetup.sh'
	exit 1
fi

source ~/.config/cachegit/settings

if [[ ! -v cachedir ]]; then
	echo 'cachegit setup invalid, inspect ~/.cachegit'
	exit 1
fi

dest=""
commitish="HEAD" # TODO handle default commitish

while [[ -n "$1" ]]; do
	case "$1" in
	-h | --help)
		echo "TODO: add help"
		exit 0
		;;
	-u | --url)
		shift
		repourl="$1"
		if [[ -z "$dest" ]]; then
			dest="$repourl"
			dest="${dest//\\/\/}"
			dest="${dest##*/}"
			dest="${dest%.git}"
		fi
		;;
	-c | --commitish)
		shift
		commitish="$1"
		;;
	-d | --dest)
		shift
		dest="$1"
		;;
	esac

	shift
done

if [[ -z "$repourl" || -z "$dest" || -z "$commitish" ]]; then
	echo "Usage $0 -u clone-url -c commit-ish [-d dest-path]"
	exit 1
fi

wd="$(pwd)"

safeurl="${repourl//[@:\/]/#}"
cd "$cachedir/union.git"
git remote add "$safeurl" "$repourl" || true
git fetch -p --tags --filter=blob:none "$safeurl"

cd "$wd"
mkdir -p "$dest"
cd "$dest"
destfullpath="$(pwd)"
if [[ -e ".git" ]];then
	echo "repo already initalized"
	exit 1
fi
git init
git config gc.auto 0
cd git
rm -rf objects
relpathobj="$(realpath --relative-to=. "$cachedir/obj")"
ln -s "$relpathobj" objects
cd ..
git remote add origin "$repourl"
git fetch -p --all --tags --filter=blob:none
git checkout "$commitish"

echo "$destfullpath" >>"$cachedir/list"
