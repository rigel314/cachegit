if [[ ! -f ~/.config/cachegit/settings ]]; then
	echo 'cachgit not setup, run initialsetup.sh'
	exit 1
fi

source ~/.config/cachegit/settings

if [[ ! -v cachedir ]]; then
	echo 'cachegit setup invalid, inspect ~/.cachegit'
	exit 1
fi

scriptdir="$(dirname "${BASH_SOURCE[0]}")"

recursive="false"
submodules=()

while [[ -n "$1" ]]; do
	case "$1" in
	-h | --help)
		echo "TODO: add help"
		exit 0
		;;
	-r | --recursive)
		recursive="true"
		;;
	*)
		submodules+=("$1")
		;;
	esac

	shift
done

gitdir="$(git rev-parse --show-toplevel)"
wd="$(pwd)"

if [[ "${#submodules[@]}" -eq 0 ]]; then
	for path_key in $(git config --file "$gitdir/.gitmodules" --name-only --get-regexp path); do
		submodules+=("$(git config --file "$gitdir/.gitmodules" "$path_key")")
	done
fi

if [[ "${#submodules[@]}" -eq 0 ]]; then
	echo "no submodules"
	exit 1
fi

if ! topurl="$(git remote get-url origin)"; then
	echo "no remote named origin"
	exit 1
fi

for submodule_path in "${submodules[@]}"; do
	cd "$wd"
	key="$(realpath --relative-to="$gitdir" "$gitdir/$submodule_path")"

	url="$(git config --file "$gitdir/.gitmodules" "submodule.$key.url")"
	if [[ "$url" == "."* ]]; then
		if [[ "$URL" == *"://"* ]]; then
			# url is a http(s):// scheme or something like it (file://, ssh://, etc)
			url="$(realpath -m --relative-base=. ./${topurl}/${url})"
			url="${url/:\//:\/\/}"
		else
			# url is a git@gitlab.com:group/project scheme or something like it
			url="$(realpath -m --relative-base=. ./${topurl/:/:\/}/${url})"
			url="${url/:\//:}"
		fi
	fi

	sub_contain_dir="$(dirname "$submodule_path")"
	sub_basename="$(basename "$submodule_path")"
	sha="$(git cat-file -p HEAD:"$sub_contain_dir" | grep -P "\\t$sub_basename\$" | awk '{print $3}')"

	cd "$gitdir/$submodule_path"
	"$scriptdir"/newrepo.sh --dest . -url "$url" --commitish "$sha"
done
